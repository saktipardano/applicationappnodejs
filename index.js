const express = require("express");
const cors = require("cors");

const app = express();
const PORT = 3333;

app.use(express.urlencoded({extended: false}));

app.use(cors());

//routing
app.use(require('./routes'));

//implementing the port
app.listen(PORT, () => {
    console.log('Server Running di http://localhost:$(PORT)');
});